try {
	toastLog('启动中，请稍后....')
	launchApp('微视')
	setTimeout(() => {
		// youngIKnow();
		// 配置
		const config = {
			// 单个视频时长
			unitTime: 120,
			// 刷取时长（奖励）
			time: 120,
			// 奖励积分
			rewardScore: 40
		}
		// 记录当前积分累加时长
		let timeCount = 0
		// 记录当前视频倒计时
		let countDown = config.unitTime
		const width = device.width
		const height = device.height
		setInterval(() => {
			timeCount += 1
			countDown--
			if (countDown === 0) {
				if (!check()) {
					app.vue.stop()
					toastLog('检测异常，请重启软件')
					throw new Error()
				}
				// moshiCloseHongBao();
				// randomUpSildeScreen();
				// randomDownSildeScreen();
				// randomFollow();
				// randomHeart();
				slideScreenDown(20, height - (height * 0.15), 20, height * 0.05, 300)
				// slideScreenDown(width / 2, height - (height * 0.15), width / 2, height * 0.05, 300)
				if (timeCount >= config.time) {
					app.vue.reward(config.rewardScore)
					timeCount = 0
					toastLog('奖励已到账')
				}
				countDown = config.unitTime
			}
			toastLog(countDown + '秒下个视频')
		}, 1000)
	}, 5000)
} catch (e) {
	toastLog('任务异常，请重启软件：' + e)
	app.vue.stop()
}

function check() {
	return (text("关注").exists() && text("推荐").exists() && text("首页").exists())
}

function youngIKnow() {
	if (className("android.widget.Button").text("知道了").exists()) {
		className("android.widget.ImageView").text("知道了").findOnce().click();
	}
}
/**
 * 屏幕向下滑动并延迟8至12秒
 */
function slideScreenDown(startX, startY, endX, endY, pressTime) {
	swipe(startX, startY, endX, endY, pressTime);
	// let delayTime = random(5000, 8000);
	// sleep(delayTime);
}

function moshiCloseHongBao() {
	if (className("android.widget.ImageView").id("img_close").exists()) {
		className("android.widget.ImageView").id("img_close").findOnce().click();
	}
}
/**
 * 随机上滑（防止被判定是机器）上滑后停留时间至少是10S，造成假象表示是对内容感兴趣
 * 点赞和关注先不搞。
 */
function randomUpSildeScreen() {
	let randomIndex = random(1, 40);
	if (randomIndex == 1) {
		console.log("随机上滑被执行了");
		pressTime = random(200, 500);
		swipe(device.width / 2, 500, device.width / 2, device.height - 200, 700);
		delayTime = random(10000, 15000);
		sleep(delayTime);
	}
}
/**
 * 连续下滑对上一个无兴趣
 * 其实得和上滑做个排他，既然无兴趣不要在上滑
 */
function randomDownSildeScreen() {
	let randomIndex = random(1, 50);
	if (randomIndex == 1) {
		console.log("连续下滑被执行了");
		swipe(device.width / 2, device.height - 200, device.width / 2, 500, 700);
		sleep(2000);
		swipe(device.width / 2, device.height - 200, device.width / 2, 500, 700);
		delayTime = random(8000, 10000);
		sleep(delayTime);
	}
}

/**随机点赞并休息一秒 */
function randomHeart() {
	index = random(1, 50);
	if (index == 6) {
		var target = id('a4l').findOnce();
		if (target == null) {
			return;
		} else {
			target.click();
			sleep(1000);
			console.log("随机点赞并休息一秒");
		}
	}
}
/**
 * 随机关注
 */
function randomFollow() {
	index = random(1, 100);
	if (index == 66) {
		var target = id('a4j').findOnce();
		if (target == null) {
			return;
		} else {
			target.click();
			sleep(1000);
			console.log("随机关注并休息一秒");
		}
	}
}