@echo off
%1 mshta vbscript:CreateObject("Shell.Application").ShellExecute("cmd.exe","/c %~s0 ::","","runas",1)(window.close)&&exit
cd /d "%~dp0"
:: 设置hosts文件路径
set HOSTS=C:\Windows\System32\drivers\etc\hosts
:: 判断当前路径是否存在 hosts.init 文件, 不存在则备份
if not exist %HOSTS%.init (
    copy /y %HOSTS% %HOSTS%.init && echo 系统hosts文件备份完成!
)
::
@echo
@echo 127.0.0.1 pdf.client.zcjb.com.cn >>C:\Windows\System32\drivers\etc\hosts
:: 刷新dns缓存
@ipconfig /flushdns
echo.
exit
