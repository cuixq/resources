# index

> URL: http://localhost:8080/test/?a=a_ytsrk&b=b_4yi00
>
> Origin Url: http://localhost:8080/test/
>
> Type: GET


### Request headers

|Header Name| Header Value|
|---------|------|

### Parameters

##### Path parameters

| Parameter | Type | Value | Description |
|---------|------|------|------------|


##### URL parameters

|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|
|true|a|String|a_ytsrk||
|true|b|String|b_4yi00||


##### Body parameters

###### JSON

```

```

###### JSON document

```
null
```


##### Form URL-Encoded
|Required| Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


##### Multipart
|Required | Parameter | Type | Value | Description |
|---------|---------|------|------|------------|


### Response

##### Response example

```

```

##### Response document
```
null
```


